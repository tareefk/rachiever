#' Returns a tibble for a given comment field
#' @param comm The comments field from the achievements data frame
#' @return a tibble with all the comments for that field
#' @noRd
flatten_comment <- function(comm) {
  tibble::tibble(
    id = purrr::map_int(comm, "id"),
    comment = purrr::map_chr(comm, "comment"),
    achievement_id = purrr::map_int(comm, "achievement_id"),
    owner_id = purrr::map_int(comm, "owner_id"),
    created_time = as.POSIXct(purrr::map_chr(comm, "created_time")),
    updated_time = as.POSIXct(purrr::map_chr(comm, "updated_time"))
  )

}

#' Extracts all the comments from a given data frame of achievements
#'
#' @param achievements Data frame returned from \code{\link{get_achievements}}
#' @param users Data frame of all the users
#' @return A data frame with all the comments from the achievements data frame
#' @export
extract_comments <- function( achievements, users = get_users() ) {
  comm <- purrr::compact(achievements$comments)
  lol <- purrr::map(comm, flatten_comment)

  comments <- dplyr::bind_rows(lol)
  if (nrow(comments) > 0 ) {
    comments <- dplyr::left_join(comments, users, by="owner_id")
    comments
  }
  else {
    comments
  }

}
