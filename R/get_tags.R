achiever_url <- "https://achiever-api.rstudio.com/"

#' Returns the list of all tags defined in the system
#' @return A data frame with all the tags that are defined in the system
#' @export
get_tags <- function() {
  tag_response <- httr::GET(paste0(achiever_url,"tags"))
  httr::stop_for_status(tag_response)

  tag_content <- httr::content(tag_response)
  tags <- dplyr::bind_rows(tag_content)
  tags$date <- as.POSIXct(tags$date)
  tags
}
