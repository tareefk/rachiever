#' Returns a tibble for all the spotlights for a given spotlight field
#'
#' @param name spotlight The spotlights field from the achievements data frame.
#' @return a tibble with all the spotlights for that field.
#' @noRd
flatten_spotlights <- function(spotlight) {
  tibble::tibble(
    id = purrr::map_int(spotlight, "id"),
    achievement_id = purrr::map_int(spotlight, "achievement_id"),
    owner_id = purrr::map_int(spotlight, "owner_id"),
    created_time = as.POSIXct(purrr::map_chr(spotlight,"created_time"))
  )
}

#' Extracts all the spotlights from a given data frame of achievements.
#'
#' @param achievements Data frame returned from \code{\link{get_achievements}}
#' @param users Data frame of all the users
#' @return A data frame with all the spotlights from the achievements data frame
#' @export
extract_spotlights <- function( achievements , users = get_users()) {
  spotlight <- purrr::compact(achievements$spotlights)

  spotlights <- dplyr::bind_rows(purrr::map(spotlight, flatten_spotlights))

  if (nrow(spotlights) > 0) {
    spotlights <- dplyr::left_join(spotlights, users, by="owner_id")
    spotlights
  } else {
    spotlights
  }

}
