# rachiever

A package that provides an R interface to the achiever REST-based APIs.

## Configuration
To get started, do the following:

1. If you don't already have one, generate a GitHub personal access token [here](https://github.com/settings/tokens).

2. Install the package from github using your GitHub PAT:
```
# install.packages("devtools")
devtools::install_github("rstudio/rachiever", auth_token = "<your GitHub PAT>")
```

3. Create a file called .Renviron in the project you are creating.

4. Get the achiever token that is assigned to you when you log into the service. The easiest way to do this is to inspect the achiever cookie in a browser.  Assign it to a variable in .Renviron, e.g.,
```
ACHIEVER_TOKEN="<token>"
```

5. Restart R to pick up the change to the environment.

## Usage
Load the library, pull in the achiever token, and you're off to the races.

```
library(rachiever)
token <- Sys.getenv("ACHIEVER_TOKEN")

achievements <- get_achievements(token,
                                 since = StartDate,
                                 until = endDate)

users <- get_users(token)

comments <- extract_comments(achievements, users)

spotlights <- extract_spotlights(achievements, users)
```

## Deployment
Today, projects that use this library can only be deployed to shinyapps.io.

1. [Enable use of private packages in your shinyapps.io account](https://support.rstudio.com/hc/en-us/articles/204536558-Enabling-use-of-private-packages-on-github-com)

2. When you deploy the project, be sure to include the .Renviron file.
