---
title: "Using rachiever"
author: "Tareef Kawaf"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Using rachiever}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

rachiever is a package that provides access to the [achiever](https://github.com/rstudio/achiever) service.  It should provide the key functions that an R user would need to understand and communicate about the achievements.  


## Usage

To access the achiever APIs, you will need to get access to the _token_.  The easiest way to get the token today is grab it from the cookie after you successfully log into the service in your browser. 

Here is a sample flow

```{r, eval=FALSE}
library(rachiever)

achievements <- get_achievements(token,
                                 since = StartDate,
                                 until = endDate)

users <- get_users()

comments <- extract_comments(achievements, users)

spotlights <- extract_spotlights(achievements, users)

```
